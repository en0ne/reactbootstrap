const path = require('path');

module.exports = {
    entry: './src/Entry.js',
    module: {
        rules: [{
            test: /\.jsx*$/,
            use: ['babel-loader'],
            exclude: /node_modules/
        }, {
            test: /\.css$/,
            use: [
                'style-loader',
                'css-loader'
            ]
        }, {
            test: /\.(png|svg|jpg|gif)$/,
            use: [
                'file-loader?outputPath=img/'
            ]
        }]
    }
};