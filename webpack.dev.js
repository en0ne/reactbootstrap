const path = require('path');
const merge = require('webpack-merge');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    mode: 'development',
    output: {
        path: path.resolve(__dirname, 'public_dev/app'),
        filename: '[name].js',
        chunkFilename: '[name].chunk.js',
        publicPath: '/'
    },
    plugins: [
        new CleanWebpackPlugin(['./public_dev']),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './src/template.html'
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin()
    ],
    devServer: {
        hot: true,
        open: true,
        host: '0.0.0.0',
        port: 14732,
        historyApiFallback: true,
        contentBase: './public_dev'
    }
});