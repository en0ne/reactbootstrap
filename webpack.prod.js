const path = require('path');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    mode: 'production',
    output: {
        path: path.resolve(__dirname, 'public/bundle'),
        filename: '[name].js',
        publicPath: '/bundle'
    },
    plugins: [
        new CleanWebpackPlugin(['./public']),
        new HtmlWebpackPlugin({
            filename: path.resolve(__dirname, 'public/index.html'),
            template: './src/template.html'
        })
    ]
});