import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import App from './App';
require('es6-promise').polyfill();
require('isomorphic-fetch');
import Root from './stores/Root';

const rootStore = new Root();

const render = Component => {
    ReactDOM.render(
        <Provider {...rootStore}>
            <Component/>
        </Provider>,
        document.getElementById('root')
    )
};

render(App);

if (module.hot) {
    module.hot.accept('./App', () => render(App));
}