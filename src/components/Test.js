import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';

import './Test.css';

@inject('counterStore')
@observer
export default class Test extends Component {
    render() {
        const {counterStore} = this.props;
        return (
            <div className="test">
                <div className="img"/>
                Test is ok. Counter {counterStore.counter}
                <button onClick={counterStore.increment}>increment</button>
            </div>
        );
    }
}