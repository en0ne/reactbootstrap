import Counter from './Counter';

export default class Root {
    constructor() {
        this.counterStore = new Counter(this);
    }
}