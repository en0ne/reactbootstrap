import { action, observable, computed } from 'mobx';

export default class Counter {
    rootStore;
    @observable counter;

    constructor(rootStore) {
        this.rootStore = rootStore;
        this.counter = 0;
    }

    @action
    increment = () => {
        this.counter++;
    }
}